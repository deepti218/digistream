import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

import { AuthService } from './services/auth.service';

import { AuthGuard } from './guards/auth.guard';
import { RandomGuard } from './guards/random.guard';

import { TokenInterceptor } from './token.interceptor';

import { LoginComponent } from './containers/login/login.component';


@NgModule({
  declarations: [LoginComponent],
  providers: [
    AuthGuard,
    AuthService,
    RandomGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  imports: [
    NgbModule,
    RouterModule,
    HttpClientModule,
    SharedModule
  ]
})
export class AuthModule { }
