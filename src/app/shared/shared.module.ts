import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { NotFoundComponent } from './components/not-found/not-found.component';

@NgModule({
  declarations: [NotFoundComponent],
  imports: [
  ],

  exports: [
    CommonModule,
    ReactiveFormsModule,
    NotFoundComponent
  ]
})
export class SharedModule { }
