import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { HomeComponent } from "./page/home.component";

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    NgbModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
